package com.api.manifesto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManifestoApplication {

	private static Logger logger = LoggerFactory.getLogger(ManifestoApplication.class);

	public static void main(String[] args) {
		logger.info("[App Manifesto] | Iniciando aplicação.....");
		SpringApplication.run(ManifestoApplication.class, args);
		logger.info("[App Manifesto] | Aplicação Iniciada.");
	}
}
