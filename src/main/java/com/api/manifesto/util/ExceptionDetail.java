package com.api.manifesto.util;

import lombok.Getter;

@Getter
public enum ExceptionDetail {

    CRIAR_EMPRESA("MANIF-0001", "Erro ao salvar empresa. ");

    ExceptionDetail(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;
}
