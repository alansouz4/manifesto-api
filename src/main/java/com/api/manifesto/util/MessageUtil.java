package com.api.manifesto.util;

public class MessageUtil {

    public static String CADASTRO_EMPRESA_PRESENTER = "Erro no cadastro.";
    public static String OBTER_EMPRESA_PRESENTER_ERRO = "Erro no cadastro.";
    public static String OBTER_EMPRESA_ENUM_ERRO = "Erro na conversão do enum para string.";
    public static String OBTER_EMPRESA_USECASE_ERRO = "Erro ao obter a empresa.";
    public static String OBTER_EMPRESA_ADAPTER_ERRO = "Erro ao obter a empresa.";
    public static String NOTFOUND_EMPRESA_USECASE = "Erro ao obter a empresa, não existe no banco.";


    public static String CADASTRO_ENDERECO_PRESENTER = "Erro no cadastro.";
    public static String OBTER_ENDERECO_PRESENTER_ERRO = "Erro no cadastro.";
    public static String OBTER_ENDERECO_USECASE_ERRO = "Erro ao obter a empresa.";
    public static String OBTER_ENDERECO_ADAPTER_ERRO = "Erro ao obter a empresa.";
    public static String NOTFOUND_ENDERECO_USECASE = "Erro ao obter a endereço, não existe no banco.";
    public static String OBTER_PAIS_ADAPTER_ERRO = "Erro ao obter o pais.";
    public static String NOTFOUND_PAIS_USECASE = "Erro ao obter pais, não existe no banco.";
}
