package com.api.manifesto.exception;

import lombok.Getter;

@Getter
public class PresenterException extends ManifestoException {

    private Integer code;
    private String message;
    private Throwable cause;

    public PresenterException(Integer code, String message) {
        super(code, String.format("[Presenter] | %s", message));
        this.code = code;
        this.message = message;
    }

    public PresenterException(String message) {
        super(message);
        this.message = message;
    }

    public PresenterException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.cause = cause;
    }

    public PresenterException(Integer code, Throwable cause) {
        super(code, cause);
        this.code = code;
        this.cause = cause;
    }

    public PresenterException(Integer code) {
        super(code);
        this.code = code;
    }

    public PresenterException(Throwable cause) {
        super(cause);
        this.cause = cause;
    }

    public PresenterException(Integer code, String message, Throwable cause) {
        super(code, String.format("[Presenter] | %s", message), cause);
        this.code = code;
        this.message = message;
    }
}
