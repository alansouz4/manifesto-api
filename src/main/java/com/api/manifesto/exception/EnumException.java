package com.api.manifesto.exception;

import lombok.Getter;

@Getter
public class EnumException extends ManifestoException {

    private Integer code;
    private String message;
    private Throwable cause;

    public EnumException(Integer code, String message) {
        super(code, String.format("[Presenter] | %s", message));
        this.code = code;
        this.message = message;
    }

    public EnumException(Integer code, String message, Throwable cause) {
        super(code, String.format("[Presenter] | %s", message), cause);
        this.code = code;
        this.message = message;
        this.cause = cause;
    }

    public EnumException(String message) {
        super(String.format("[Presenter] | %s", message));
        this.message = message;
    }
}

