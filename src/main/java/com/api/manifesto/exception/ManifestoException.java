package com.api.manifesto.exception;

public class ManifestoException extends RuntimeException{

    private Integer code;
    private Throwable cause;
    private String message;

    public ManifestoException(Integer code, String message){
        super(message);
        this.code = code;
    }

    public ManifestoException(String message, Throwable cause){
        super(message, cause);
        this.message = message;
        this.cause = cause;
    }

    public ManifestoException(Integer code, Throwable cause){
        super(cause);
        this.code = code;
    }

    public ManifestoException(Integer code){
        this.code = code;
    }

    public ManifestoException(Throwable cause){
        super(cause);
        this.cause = cause;
    }

    public ManifestoException(Integer code, String message, Throwable cause){
        super(message, cause);
        this.code = code;
    }

    public ManifestoException(String message){
        super(message);
    }
}
