package com.api.manifesto.exception;

import lombok.Getter;

@Getter
public class UseCaseException extends ManifestoException{

    private Integer code;
    private String message;
    private Throwable cause;

    public UseCaseException(Integer code, String message) {
        super(code, String.format("[UseCase Exception] | %s", message));
        this.code = code;
        this.message = message;
    }

    public UseCaseException(String message) {
        super(message);
        this.message = message;
    }

    public UseCaseException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.cause = cause;
    }

    public UseCaseException(Integer code, Throwable cause) {
        super(code, cause);
        this.code = code;
        this.cause = cause;
    }

    public UseCaseException(Integer code) {
        super(code);
        this.code = code;
    }

    public UseCaseException(Throwable cause) {
        super( cause);
        this.cause = cause;
    }

    public UseCaseException(Integer code, String message, Throwable cause) {
        super(message);
        this.code = code;
        this.message = message;
        this.cause = cause;
    }
}
