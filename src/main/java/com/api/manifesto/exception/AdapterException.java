package com.api.manifesto.exception;

import lombok.Getter;

@Getter
public class AdapterException extends ManifestoException {

    private Integer code;
    private String message;
    private Throwable cause;

    public AdapterException(Integer code, String message) {
        super(code, String.format("[Adapter Exception] | %s", message));
        this.code = code;
        this.message = message;
    }

    public AdapterException(String message) {
        super(message);
        this.message = message;
    }

    public AdapterException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.cause = cause;
    }

    public AdapterException(Integer code, Throwable cause) {
        super(code, cause);
        this.code = code;
        this.cause = cause;
    }

    public AdapterException(Integer code) {
        super(code);
        this.code = code;
    }

    public AdapterException(Throwable cause) {
        super(cause);
        this.cause = cause;
    }

    public AdapterException(Integer code, String message, Throwable cause){
        super(code, message, cause);
        this.code = code;
        this.message = message;
        this.cause = cause;
    }
}
