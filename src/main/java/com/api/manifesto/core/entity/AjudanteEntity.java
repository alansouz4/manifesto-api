package com.api.manifesto.core.entity;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class AjudanteEntity {

    private Long id;
    private Long motoristaId;
    private String nome;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
