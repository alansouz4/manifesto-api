package com.api.manifesto.core.entity;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class HabilitacaoEntity {

    private Long id;
    private String nome;
    private LocalDateTime dataHoraCriacao;
}
