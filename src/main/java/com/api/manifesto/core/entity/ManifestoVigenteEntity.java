package com.api.manifesto.core.entity;

import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ManifestoVigenteEntity {

    private Long id;
    private String nome;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
    private Long idsEmpresas;
    private StatusAgendaManifestoEnum statusAgendaManifesto;
}
