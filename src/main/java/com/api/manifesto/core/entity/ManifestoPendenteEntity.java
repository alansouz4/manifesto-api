package com.api.manifesto.core.entity;

import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class ManifestoPendenteEntity {

    private Long id;
    private String nome;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
    private EmpresaEntity empresas;
    private StatusAgendaManifestoEnum statusAgendaManifesto;
}
