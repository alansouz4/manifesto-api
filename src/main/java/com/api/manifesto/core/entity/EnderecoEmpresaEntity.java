package com.api.manifesto.core.entity;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class EnderecoEmpresaEntity {

    private Long id;
    private Long empresaId;
    private String ruaEnumero;
    private String bairro;
    private String cidade;
    private String estado;
    private String pais;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
}
