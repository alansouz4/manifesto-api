package com.api.manifesto.core.entity;

import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class EmpresaEntity {

    private Long id;
    private Long idManifesto;
    private String nome;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
    private Set<EnderecoEmpresaEntity> enderecos;
    private Collection<MotoristaEntity> motoristas;
}
