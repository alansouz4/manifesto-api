package com.api.manifesto.core.entity;

import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class UsuarioEntity {

    private Long id;
    private String login;
    private String senha;
    private boolean admin;
}
