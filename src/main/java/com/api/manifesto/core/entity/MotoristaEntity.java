package com.api.manifesto.core.entity;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class MotoristaEntity {

    private Long id;
    private Long empresaId;
    private String nome;
    private String rg;
    private String placaVeiculo;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraManutencao;
    private Collection<AjudanteEntity> ajudantes;
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
