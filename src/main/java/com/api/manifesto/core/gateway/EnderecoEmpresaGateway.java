package com.api.manifesto.core.gateway;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface EnderecoEmpresaGateway {

    EnderecoEmpresaEntity criarEnderecoEmpresa(EnderecoEmpresaEntity entity);

    EnderecoEmpresaEntity obterEnderecoById(Long idEndereco);

    Set<EnderecoEmpresaEntity> obterEnderecoPorIdEmpresa(Long idEmpresa);

    Collection<EnderecoEmpresaEntity> findPais(String pais);

    List<EnderecoEmpresaEntity> findAll();

    EnderecoEmpresaEntity alterarPais(Long idEndereco, EnderecoEmpresaEntity entity);

    void deletarEnderecoPorId(Long id);
}
