package com.api.manifesto.core.gateway;

import com.api.manifesto.core.entity.EmpresaEntity;

import javax.naming.ServiceUnavailableException;
import java.util.Collection;

public interface EmpresaGateway {

    EmpresaEntity criarEmpresa(EmpresaEntity entity);

    EmpresaEntity obterEmpresaById(Long id);

    Collection<EmpresaEntity> obterEmpresaByIdManifesto(Long idManifesto);

    void deletarEmpresaPorId(Long id);
}
