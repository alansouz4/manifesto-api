package com.api.manifesto.core.gateway;

import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.enums.CategoriaHabilitacaoEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface MotoristaGateway {

    void criarMotorista(MotoristaEntity motoristaEntity);

    List<String> obterNomeMotorista();

    MotoristaEntity obterMotoristaPorId(Long id);

    Collection<MotoristaEntity> obterMotoristaPorIdEmpresa(Long idEmpresa);

//    Collection<MotoristaEntity> obterMotoristaPorStatus(String status, Long idMotorista);

    Collection<MotoristaEntity> obterMotoristaPorData(LocalDateTime dataInicial, LocalDateTime dataFinal);

    void deletarMotorista(Long id);
}
