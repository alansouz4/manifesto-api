package com.api.manifesto.core.gateway;

public interface AjudanteGateway {

    void deletarAjudantePorId(Long id);
}
