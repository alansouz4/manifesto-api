package com.api.manifesto.core.gateway;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.entity.ManifestoVigenteEntity;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;

public interface ManifestoGateway {

    ManifestoPendenteEntity criarManifesto(ManifestoPendenteEntity entity);

    ManifestoPendenteEntity obterManifestoPorIdEStatus(Long id, StatusAgendaManifestoEnum status);

    void deletarManifestoPendentePorIdEStatus(Long id, StatusAgendaManifestoEnum status);

    ManifestoVigenteEntity salvarManifestoVigente(ManifestoVigenteEntity entity);
}
