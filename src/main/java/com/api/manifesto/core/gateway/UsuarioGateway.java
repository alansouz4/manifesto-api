package com.api.manifesto.core.gateway;

import com.api.manifesto.core.entity.UsuarioEntity;

public interface UsuarioGateway {

    UsuarioEntity criarUsuario(UsuarioEntity entity);

    UsuarioEntity obterUsuario(String login);
}
