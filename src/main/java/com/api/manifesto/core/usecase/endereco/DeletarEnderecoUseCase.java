package com.api.manifesto.core.usecase.endereco;

import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.core.gateway.EnderecoEmpresaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class DeletarEnderecoUseCase {

    private final EnderecoEmpresaGateway enderecoEmpresaGateway;

    public DeletarEnderecoUseCase(EnderecoEmpresaGateway enderecoEmpresaGateway) {
        this.enderecoEmpresaGateway = enderecoEmpresaGateway;
    }

    public void deletarEnderecoPorId(Long id) {
        try {
            enderecoEmpresaGateway.deletarEnderecoPorId(id);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e);
        }
    }
}
