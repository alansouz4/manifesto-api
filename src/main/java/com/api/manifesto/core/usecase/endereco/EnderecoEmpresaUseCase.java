package com.api.manifesto.core.usecase.endereco;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.core.gateway.EnderecoEmpresaGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Component
public class EnderecoEmpresaUseCase {

    private EnderecoEmpresaGateway gateway;

    @Autowired
    public EnderecoEmpresaUseCase(EnderecoEmpresaGateway gateway) {
        this.gateway = gateway;
    }

    public void criarEnderecoEmpresa(EnderecoEmpresaEntity entity) {
        entity.setDataHoraCriacao(LocalDateTime.now());
        gateway.criarEnderecoEmpresa(entity);
    }

    public EnderecoEmpresaEntity obterEnderecoEmpresaPorId(Long id) {
        return gateway.obterEnderecoById(id);
    }

    public Collection<EnderecoEmpresaEntity> findPais(String pais) {
        return gateway.findPais(pais);
    }

    public List<EnderecoEmpresaEntity> findAll() {
        return gateway.findAll();
    }

    public EnderecoEmpresaEntity atualizarPais(Long idEndereco, EnderecoEmpresaEntity entity){
        return gateway.alterarPais(idEndereco, entity);
    }
}
