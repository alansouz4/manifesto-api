package com.api.manifesto.core.usecase.empresa;

import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.core.gateway.EnderecoEmpresaGateway;
import com.api.manifesto.core.gateway.MotoristaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.util.MessageUtil;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

@Component
public class ObterEmpresaPorIdUseCase {

    private final EmpresaGateway empresaGateway;
    private final MotoristaGateway motoristaGateway;
    private final EnderecoEmpresaGateway enderecoEmpresaGateway;

    public ObterEmpresaPorIdUseCase(EmpresaGateway empresaGateway,
                                    MotoristaGateway motoristaGateway,
                                    EnderecoEmpresaGateway enderecoEmpresaGateway) {
        this.empresaGateway = empresaGateway;
        this.motoristaGateway = motoristaGateway;
        this.enderecoEmpresaGateway = enderecoEmpresaGateway;
    }

    public EmpresaEntity obterEmpresaById(Long idEmpresa) {
        try {
            Set<EnderecoEmpresaEntity> enderecoEntity = enderecoEmpresaGateway.obterEnderecoPorIdEmpresa(idEmpresa);
            Collection<MotoristaEntity> motoristaEntity = motoristaGateway.obterMotoristaPorIdEmpresa(idEmpresa);
            EmpresaEntity entity = empresaGateway.obterEmpresaById(idEmpresa);
            entity.setEnderecos(enderecoEntity);
            entity.setMotoristas(motoristaEntity);
            return entity;
        } catch (AdapterException e) {
            throw new UseCaseException(500, MessageUtil.OBTER_EMPRESA_USECASE_ERRO);
        }
    }
}
