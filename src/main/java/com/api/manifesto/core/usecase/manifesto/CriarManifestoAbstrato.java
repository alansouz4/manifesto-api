package com.api.manifesto.core.usecase.manifesto;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;

public abstract class CriarManifestoAbstrato {

    public abstract ManifestoPendenteEntity criarManifesto(ManifestoPendenteEntity entity);

    public Object find() {
        return null;
    }
}
