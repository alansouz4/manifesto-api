package com.api.manifesto.core.usecase.empresa;

import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class DeletarEmpresaPorIdUseCase {

    private final EmpresaGateway empresaGateway;

    public DeletarEmpresaPorIdUseCase(EmpresaGateway empresaGateway) {
        this.empresaGateway = empresaGateway;
    }

    public void deletarEmpresaPorId(Long id) {
        try {
            empresaGateway.deletarEmpresaPorId(id);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }
}
