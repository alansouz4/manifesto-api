package com.api.manifesto.core.usecase.empresa;

import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.core.gateway.EnderecoEmpresaGateway;
import com.api.manifesto.core.gateway.MotoristaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;

@Component
public class CriarEmpresaUseCase {

    private final EmpresaGateway empresaGateway;
    private final MotoristaGateway motoristaGateway;
    private final EnderecoEmpresaGateway enderecoEmpresaGateway;

    @Autowired
    public CriarEmpresaUseCase(EmpresaGateway empresaGateway,
                               MotoristaGateway motoristaGateway,
                               EnderecoEmpresaGateway enderecoEmpresaGateway) {
        this.empresaGateway = empresaGateway;
        this.motoristaGateway = motoristaGateway;
        this.enderecoEmpresaGateway = enderecoEmpresaGateway;
    }


    public EmpresaEntity criarEmpresa(EmpresaEntity entity) {
        try {
            entity.setDataHoraCriacao(LocalDateTime.now());
            return empresaGateway.criarEmpresa(entity);
        }catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e.getCause());
        }
    }
}
