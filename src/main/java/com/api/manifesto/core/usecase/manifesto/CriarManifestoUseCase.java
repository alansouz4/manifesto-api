package com.api.manifesto.core.usecase.manifesto;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.core.gateway.ManifestoGateway;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CriarManifestoUseCase extends CriarManifestoAbstrato {

    private final EmpresaGateway empresaGateway;
    private final ManifestoGateway manifestoGateway;

    public CriarManifestoUseCase(EmpresaGateway empresaGateway, ManifestoGateway manifestoGateway) {
        this.empresaGateway = empresaGateway;
        this.manifestoGateway = manifestoGateway;
    }

    public ManifestoPendenteEntity criarManifesto(ManifestoPendenteEntity entity){
        try {
            entity.setStatusAgendaManifesto(StatusAgendaManifestoEnum.AGUARDANDO_APROVACAO);
            entity.setDataHoraCriacao(LocalDateTime.now());
            return manifestoGateway.criarManifesto(entity);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e.getCause());
        }
    }

}
