package com.api.manifesto.core.usecase.motorista;

import com.api.manifesto.core.gateway.MotoristaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class DeletarMotoristaUseCase {

    private final MotoristaGateway motoristaGateway;

    public DeletarMotoristaUseCase(MotoristaGateway motoristaGateway) {
        this.motoristaGateway = motoristaGateway;
    }

    public void deletarMotorista(Long id) {
        try {
            motoristaGateway.deletarMotorista(id);
        } catch (AdapterException e) {
            throw new UseCaseException(503, e.getMessage());
        }
    }
}
