package com.api.manifesto.core.usecase.motorista;

import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.core.gateway.MotoristaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Component
public class MotoristaUseCase {

    private final MotoristaGateway motoristaGateway;

    public MotoristaUseCase(MotoristaGateway motoristaGateway) {
        this.motoristaGateway = motoristaGateway;
    }

    public void criarMotorista(MotoristaEntity motoristaEntity){
        try {
            motoristaEntity.setDataHoraCriacao(LocalDateTime.now());
            motoristaGateway.criarMotorista(motoristaEntity);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }

    public MotoristaEntity obterMotoristaPorId(Long id) {
        try {
            MotoristaEntity entity = motoristaGateway.obterMotoristaPorId(id);
            return entity;
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }

    public List<String> obterNomeMotorista() {
        try {
            return motoristaGateway.obterNomeMotorista();
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }

    public Collection<MotoristaEntity> obterMotoristaPorIdEmpresa(Long idEmpresa) {
        try {
            Collection<MotoristaEntity> entity = motoristaGateway.obterMotoristaPorIdEmpresa(idEmpresa);
            return entity;
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }

//    public Collection<MotoristaEntity> obterMotoristaPorStatus(String status, Long idMotorista) {
//        try {
//            Collection<MotoristaEntity> entity = motoristaGateway.obterMotoristaPorStatus(status, idMotorista);
//            return entity;
//        } catch (AdapterException e) {
//            throw new UseCaseException(e.getCode(), e.getMessage());
//        }
//    }

    public Collection<MotoristaEntity> obterMotoristaPorData(LocalDateTime dataInicial, LocalDateTime dataFinal) {
        try {
            Collection<MotoristaEntity> entity = motoristaGateway.obterMotoristaPorData(dataInicial, dataFinal);
            return entity;
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage());
        }
    }
}
