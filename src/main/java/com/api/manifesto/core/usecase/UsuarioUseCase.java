//package com.api.manifesto.core.usecase;
//
//import com.api.manifesto.core.entity.UsuarioEntity;
//import com.api.manifesto.core.gateway.UsuarioGateway;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//@Service
//public class UsuarioUseCase implements UserDetailsService {
//
//    @Autowired
//    private UsuarioGateway usuarioGateway;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        UsuarioEntity usuario = usuarioGateway.obterUsuario(username);
//
//        String[] roles = usuario.isAdmin() ?
//                new String[]{"ADMIN", "USER"} : new String[]{"USER"};
//
//        return User
//                .builder()
//                .username(usuario.getLogin())
//                .password(usuario.getSenha())
//                .roles(roles)
//                .build();
//    }
//
//    @Transactional
//    public UsuarioEntity criarUsuario(UsuarioEntity entity) {
//        return usuarioGateway.criarUsuario(entity);
//    }
//
//}
