package com.api.manifesto.core.usecase.manifesto;

import com.api.manifesto.core.gateway.ManifestoGateway;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class DeletarManifestoPendentePorIdEStatusUseCase {

    private final ManifestoGateway manifestoGateway;

    public DeletarManifestoPendentePorIdEStatusUseCase(ManifestoGateway manifestoGateway) {
        this.manifestoGateway = manifestoGateway;
    }

    public void deletarManifestoPendentePorIdEStatus(Long id) {
        try {
            manifestoGateway.deletarManifestoPendentePorIdEStatus(id, StatusAgendaManifestoEnum.AGUARDANDO_APROVACAO);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e);
        }
    }
}
