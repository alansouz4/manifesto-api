package com.api.manifesto.core.usecase.ajudante;

import com.api.manifesto.core.gateway.AjudanteGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class DeletarAjudanteUseCase {

    private final AjudanteGateway ajudanteGateway;

    public DeletarAjudanteUseCase(AjudanteGateway ajudanteGateway) {
        this.ajudanteGateway = ajudanteGateway;
    }

    public void deletarAjudantePorId( Long id ) {
        try {
            ajudanteGateway.deletarAjudantePorId(id);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getMessage());
        }
    }
}
