package com.api.manifesto.core.usecase.manifesto;

import com.api.manifesto.core.entity.*;
import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.core.gateway.ManifestoGateway;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.mappers.ManifestoPresentableMapper;
import com.api.manifesto.mappers.presentable.EmpresaPresentableMapper;
import com.api.manifesto.mappers.table.ManifestoTableMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AprovarAgendaManifestoUseCase {

    private final ManifestoGateway manifestoGateway;
    private final EmpresaGateway empresaGateway;

    public AprovarAgendaManifestoUseCase(ManifestoGateway manifestoGateway, EmpresaGateway empresaGateway) {
        this.manifestoGateway = manifestoGateway;
        this.empresaGateway = empresaGateway;
    }

    public void aprovarAgendaManifesto(Long idManifesto) {
        try {
            //buca manifesto com status aguardando aprovação
            ManifestoPendenteEntity manifestEentityPendente =
                    manifestoGateway.obterManifestoPorIdEStatus(idManifesto,
                    StatusAgendaManifestoEnum.AGUARDANDO_APROVACAO);

            // mapea o objeto pendente para vigente
            ManifestoVigenteEntity manifestoEntityVigente =
                    ManifestoTableMapper.INSTANCE.pendenteToEntityVigente(manifestEentityPendente);

            // verifica se status é aguardando aprovação
            if (manifestEentityPendente.getStatusAgendaManifesto().equals(StatusAgendaManifestoEnum.AGUARDANDO_APROVACAO)) {

                Collection<EmpresaEntity> empresaEntity = empresaGateway.obterEmpresaByIdManifesto(idManifesto);
                Collection<Long> idsEmpresas = new ArrayList<>();
                Long idEmpresa = null;
                for (EmpresaEntity entityEmpresa : empresaEntity) {
                    idEmpresa = entityEmpresa.getId();
                }
                idsEmpresas.add(idEmpresa);

                // seta aprovado no objeto vigente e salva na tabela vigente
                manifestoEntityVigente.setStatusAgendaManifesto(StatusAgendaManifestoEnum.APROVADO);
                manifestoEntityVigente.setDataHoraManutencao(LocalDateTime.now());
                manifestoEntityVigente.setDataHoraCriacao(manifestEentityPendente.getDataHoraCriacao());
                manifestoEntityVigente.setIdsEmpresas(idEmpresa);
                manifestoGateway.salvarManifestoVigente(manifestoEntityVigente);

                // deleta registro pendente da tabela pendente
                manifestoGateway.deletarManifestoPendentePorIdEStatus(manifestEentityPendente.getId(),
                        StatusAgendaManifestoEnum.AGUARDANDO_APROVACAO);
            } else {
                throw new UseCaseException(503, "Erro: Manifesto pendente esta como status aprovado");
            }

        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e);
        }
    }
}
