package com.api.manifesto.core.usecase.manifesto;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.gateway.ManifestoGateway;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.exception.UseCaseException;
import org.springframework.stereotype.Component;

@Component
public class ObterManifestoPorIdEStatusUseCase {

    private final ManifestoGateway manifestoGateway;

    public ObterManifestoPorIdEStatusUseCase(ManifestoGateway manifestoGateway) {
        this.manifestoGateway = manifestoGateway;
    }

    public ManifestoPendenteEntity obterManifestoPorIdEStatus(Long id, StatusAgendaManifestoEnum status) {
        try {
            return manifestoGateway.obterManifestoPorIdEStatus(id, status);
        } catch (AdapterException e) {
            throw new UseCaseException(e.getCode(), e.getMessage(), e);
        }
    }
}
