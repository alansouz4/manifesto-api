package com.api.manifesto.enums;

import lombok.Getter;

public enum StatusAgendaManifestoEnum {

    APROVADO(0L), AGUARDANDO_APROVACAO(1L);

    @Getter
    private Long codigo;

    StatusAgendaManifestoEnum(Long codigo) {
        this.codigo = codigo;
    }
}
