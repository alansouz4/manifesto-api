package com.api.manifesto.enums;

import com.api.manifesto.exception.EnumException;
import com.api.manifesto.util.MessageUtil;

public enum CategoriaHabilitacaoEnum {

    A(0L, "Moto"),
    B(1L, "Carro"),
    C(2L, "Caminhão"),
    D(3L, "Ônibus"),
    E(4L, "Articulado");

    private Long codigo;
    private String descricao;

    CategoriaHabilitacaoEnum(Long codigo, String descricao){
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public static String enumToString(CategoriaHabilitacaoEnum categoriaHabilitacaoEnum){
        if (categoriaHabilitacaoEnum != null) {
            CategoriaHabilitacaoEnum cat =
                    CategoriaHabilitacaoEnum.valueOf(categoriaHabilitacaoEnum.toString());
            return cat.toString();
        }
        throw new EnumException(503, MessageUtil.OBTER_EMPRESA_ENUM_ERRO);
    }
}
