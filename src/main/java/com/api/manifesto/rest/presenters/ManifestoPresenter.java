package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.usecase.manifesto.CriarManifestoUseCase;
import com.api.manifesto.core.usecase.manifesto.DeletarManifestoPendentePorIdEStatusUseCase;
import com.api.manifesto.core.usecase.manifesto.ObterManifestoPorIdEStatusUseCase;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.mappers.ManifestoPresentableMapper;
import com.api.manifesto.rest.presentables.ManifestoPendentePresentable;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/manifestos")
public class ManifestoPresenter {

    private final CriarManifestoUseCase criarManifestoUseCase;
    private final ObterManifestoPorIdEStatusUseCase obterManifestoPorIdEStatusUseCase;
    private final DeletarManifestoPendentePorIdEStatusUseCase deletarManifestoPendentePorIdEStatusUseCase;

    public ManifestoPresenter(CriarManifestoUseCase criarManifestoUseCase,
                              ObterManifestoPorIdEStatusUseCase obterManifestoPorIdEStatusUseCase,
                              DeletarManifestoPendentePorIdEStatusUseCase deletarManifestoPendentePorIdEStatusUseCase) {
        this.criarManifestoUseCase = criarManifestoUseCase;
        this.obterManifestoPorIdEStatusUseCase = obterManifestoPorIdEStatusUseCase;
        this.deletarManifestoPendentePorIdEStatusUseCase = deletarManifestoPendentePorIdEStatusUseCase;
    }

    @ApiOperation(value="Criar um novo cadastro de manifesto")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucesso")})
    @PostMapping
    public ResponseEntity<ManifestoPendentePresentable> criarManifesto(@RequestBody ManifestoPendentePresentable presentable) {
        try {
            ManifestoPendenteEntity entity = criarManifestoUseCase.criarManifesto(ManifestoPresentableMapper.INSTANCE.toEntity(presentable));
            return ResponseEntity.ok(ManifestoPresentableMapper.INSTANCE.toPresentable(entity));
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e.getCause());
        }
    }

    @ApiOperation(value="Obter manifesto por id e status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping
    public ResponseEntity<ManifestoPendentePresentable> obterManifestoPorIdEStatus(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "status")StatusAgendaManifestoEnum status) {
        try {
            ManifestoPendenteEntity entity =
                    obterManifestoPorIdEStatusUseCase.obterManifestoPorIdEStatus(id, status);
            return ResponseEntity.ok(ManifestoPresentableMapper.INSTANCE.toPresentable(entity));
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e.getCause());
        }
    }

    @ApiOperation(value="Deletar manifesto por id e status")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping
    public ResponseEntity<Void> deletarManifestoPorIdEStatus(
            @RequestParam(value = "id") Long id) {
        try {
            deletarManifestoPendentePorIdEStatusUseCase.deletarManifestoPendentePorIdEStatus(id);
            return ResponseEntity.noContent().build();
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e.getCause());
        }
    }
}
