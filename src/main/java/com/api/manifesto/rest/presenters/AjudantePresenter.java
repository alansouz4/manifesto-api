package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.usecase.ajudante.DeletarAjudanteUseCase;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ajudantes")
public class AjudantePresenter {

    private final DeletarAjudanteUseCase deletarAjudanteUseCase;

    public AjudantePresenter(DeletarAjudanteUseCase deletarAjudanteUseCase) {
        this.deletarAjudanteUseCase = deletarAjudanteUseCase;
    }

    @ApiOperation(value = "Deletar ajudante por id")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping
    public void delete(@RequestParam(name = "id") Long id) {
        try {
            deletarAjudanteUseCase.deletarAjudantePorId(id);
        } catch (UseCaseException e) {
            throw new PresenterException(e.getMessage());
        }
    }
}
