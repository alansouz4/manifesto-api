package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.core.usecase.endereco.DeletarEnderecoUseCase;
import com.api.manifesto.core.usecase.endereco.EnderecoEmpresaUseCase;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.rest.presentables.EnderecoEmpresaPresentable;
import com.api.manifesto.mappers.presentable.EnderecoEmpresaPresentableMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@Api(value="Cadastro endereço")
@RestController
@RequestMapping(value = "/enderecos")
public class EnderecoEmpresaPresenter {

    private final EnderecoEmpresaUseCase useCase;
    private final DeletarEnderecoUseCase deletarEnderecoPorId;

    public EnderecoEmpresaPresenter(EnderecoEmpresaUseCase useCase,
                                    DeletarEnderecoUseCase deletarEnderecoPorId) {
        this.useCase = useCase;
        this.deletarEnderecoPorId = deletarEnderecoPorId;
    }

    @ApiOperation(value="Cria endereço")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucesso")})
    @PostMapping
    public ResponseEntity<Void> criarEnderecoEmpresa(@RequestBody EnderecoEmpresaPresentable httpModel) {
        useCase.criarEnderecoEmpresa(EnderecoEmpresaPresentableMapper.INSTANCE.toEntity(httpModel));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value="Obtem endereço por id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("/{id}")
    public ResponseEntity<EnderecoEmpresaEntity> obeterEnderecoEmpresaPorId(@PathVariable Long id) {
        EnderecoEmpresaEntity entity = useCase.obterEnderecoEmpresaPorId(id);
        return ResponseEntity.ok(entity);
    }

    @ApiOperation(value="Obtem endereço por pais")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("/pais")
    public ResponseEntity<Collection<EnderecoEmpresaEntity>> findPais(@RequestParam("pais") String pais) {
        Collection<EnderecoEmpresaEntity> entity = useCase.findPais(pais);
        return ResponseEntity.ok(entity);
    }

    @ApiOperation(value="Obtem endereços")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping
    public ResponseEntity<List<EnderecoEmpresaEntity>> findAll() {
        List<EnderecoEmpresaEntity> entity = useCase.findAll();
        return ResponseEntity.ok(entity);
    }

    @ApiOperation(value = "Atualizar Endereco")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @PutMapping("/{idEndereco}")
    public ResponseEntity<EnderecoEmpresaEntity> atualizar(@PathVariable Long idEndereco,
                                                           @RequestBody EnderecoEmpresaPresentable presentable) {
        EnderecoEmpresaEntity entity =
        useCase.atualizarPais(idEndereco, EnderecoEmpresaPresentableMapper.INSTANCE.toEntity(presentable));
        return ResponseEntity.ok(entity);
    }

    @ApiOperation(value = "Deleta endereço por id")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping
    public void deletarEnderecoPorId(@RequestParam(name = "id") Long id) {
        try {
            deletarEnderecoPorId.deletarEnderecoPorId(id);
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e);
        }
    }
}
