package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.core.usecase.motorista.DeletarMotoristaUseCase;
import com.api.manifesto.core.usecase.motorista.MotoristaUseCase;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.rest.presentables.MotoristaPresentable;
import com.api.manifesto.mappers.presentable.MotoristaPresentableMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Api(value="Cadastro motorista")
@RestController
@RequestMapping(value = "/motoristas")
public class MotoristaPresenter {

    private final MotoristaUseCase motoristaUseCase;
    private final DeletarMotoristaUseCase deletarMotoristaUseCase;

    public MotoristaPresenter(MotoristaUseCase motoristaUseCase,
                              DeletarMotoristaUseCase deletarMotoristaUseCase) {
        this.motoristaUseCase = motoristaUseCase;
        this.deletarMotoristaUseCase = deletarMotoristaUseCase;
    }

    @ApiOperation(value="Criar um novo cadastro de motorista")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucesso")})
    @PostMapping
    public ResponseEntity<Void> criarMotorista(@RequestBody MotoristaPresentable motoristaPresentable){
        try {
            motoristaUseCase.criarMotorista(MotoristaPresentableMapper.INSTANTE.toEntity(motoristaPresentable));
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }

    @ApiOperation(value="Obter motoristas por id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("/{id}")
    public ResponseEntity<MotoristaPresentable> obterMotoristaPorId(@PathVariable Long id){
        try {
            MotoristaEntity entity = motoristaUseCase.obterMotoristaPorId(id);
            return ResponseEntity.ok(MotoristaPresentableMapper.INSTANTE.toPresentable(entity));
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }

    @ApiOperation(value="Obter nome motoristas")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("/nome")
    public ResponseEntity<List<String>> obterNomeMotorista(){
        try {
            return ResponseEntity.ok(motoristaUseCase.obterNomeMotorista());
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }

    @ApiOperation(value="Obter motoristas por idEmpresa")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("idEmpresa/{idEmpresa}")
    public ResponseEntity<Collection<MotoristaPresentable>> obterMotoristaPorIdEmpresa(
            @PathVariable Long idEmpresa){
        try {
            Collection<MotoristaEntity> entity = motoristaUseCase.obterMotoristaPorIdEmpresa(idEmpresa);
            return ResponseEntity.ok(MotoristaPresentableMapper.INSTANTE.toPresentableCollection(entity));
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }

//    @ApiOperation(value="Obter motoristas por Status")
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
//    @GetMapping("/status")
//    public ResponseEntity<Collection<MotoristaPresentable>> obterMotoristaPorStatus(
//            @RequestParam(name = "status", required = false) String status,
//            @RequestParam(name = "idMotorista", required = false) Long idMotorista){
//        try {
//            Collection<MotoristaEntity> entity = motoristaUseCase.obterMotoristaPorStatus(status, idMotorista);
//            return ResponseEntity.ok(MotoristaPresentableMapper.INSTANTE.toPresentableCollection(entity));
//        } catch (UseCaseException e) {
//            throw new PresenterException(e.getCode(), e.getMessage());
//        }
//    }

    @ApiOperation(value="Obter motoristas por data")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping
    public ResponseEntity<Collection<MotoristaPresentable>> obterMotoristaPorData(
            @RequestParam(name = "dataInicial", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dataInicial,
            @RequestParam(name = "dataFinal", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dataFinal){
        try {
            Collection<MotoristaEntity> entity = motoristaUseCase.obterMotoristaPorData(dataInicial, dataFinal);
            return ResponseEntity.ok(MotoristaPresentableMapper.INSTANTE.toPresentableCollection(entity));
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }

    @ApiOperation(value = "Deletar um motorista")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping
    public void deletarMotorista(@RequestParam(name = "id") Long id) {
        try {
            deletarMotoristaUseCase.deletarMotorista(id);
        } catch (UseCaseException e) {
            throw new PresenterException(503, e.getMessage());
        }
    }
}
