package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.core.usecase.empresa.DeletarEmpresaPorIdUseCase;
import com.api.manifesto.core.usecase.empresa.CriarEmpresaUseCase;
import com.api.manifesto.core.usecase.empresa.ObterEmpresaPorIdUseCase;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import com.api.manifesto.mappers.presentable.EmpresaPresentableMapper;
import com.api.manifesto.rest.presentables.EmpresaPresentable;
import com.api.manifesto.util.LoggerMessageUtil;
import com.api.manifesto.util.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value="Cadastro Empresa")
@RestController
@RequestMapping(value = "/empresas")
public class EmpresaPresenter {

    Logger logger = LoggerFactory.getLogger(EmpresaPresenter.class);

    private final CriarEmpresaUseCase useCase;
    private final DeletarEmpresaPorIdUseCase deletarEmpresaPorId;
    private final ObterEmpresaPorIdUseCase obterEmpresaPorIdUseCase;

    public EmpresaPresenter(CriarEmpresaUseCase useCase,
                            DeletarEmpresaPorIdUseCase deletarEmpresaPorId,
                            ObterEmpresaPorIdUseCase obterEmpresaPorIdUseCase) {
        this.useCase = useCase;
        this.deletarEmpresaPorId = deletarEmpresaPorId;
        this.obterEmpresaPorIdUseCase = obterEmpresaPorIdUseCase;
    }

    @ApiOperation(value="Cria empresa")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @PostMapping
    public ResponseEntity<EmpresaEntity> criarEmpresa(@RequestBody EmpresaPresentable httpModel) {
        try {
            logger.info("[Presenter] | Inicio cadastro empresa e endereço");
            EmpresaEntity entity = useCase.criarEmpresa(EmpresaPresentableMapper.INSTANCE.toEntity(httpModel));
            return ResponseEntity.status(HttpStatus.CREATED).body(entity);
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e.getCause());
        }
    }

    @ApiOperation(value="Obtem empresa por id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping("/{id}")
    public ResponseEntity<EmpresaPresentable> obterEmpresaById(Long id) {
        logger.info(LoggerMessageUtil.OBTER_EMPRESA_INICIO);
        EmpresaEntity entity;
        try {
            entity = obterEmpresaPorIdUseCase.obterEmpresaById(id);
            if (entity == null) {
                logger.info(String.format("%s. Code: %d", LoggerMessageUtil.NO_COTENT, 204));
                ResponseEntity.noContent().build();
            }
        } catch (UseCaseException e) {
            throw new PresenterException(500, MessageUtil.OBTER_EMPRESA_PRESENTER_ERRO);
        }
        logger.info(LoggerMessageUtil.RETORNO_EMPRESA_SUCESSO);
        return ResponseEntity.ok(EmpresaPresentableMapper.INSTANCE.toHttpModel(entity));
    }

    @ApiOperation(value = "Deleta empresa por id")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
    @DeleteMapping
    public void deletarEmpresaPorId(@RequestParam(name = "id") Long id) {
        try {
            deletarEmpresaPorId.deletarEmpresaPorId(id);
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage());
        }
    }
}
