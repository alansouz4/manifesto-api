package com.api.manifesto.rest.presenters;

import com.api.manifesto.core.usecase.manifesto.AprovarAgendaManifestoUseCase;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.PresenterException;
import com.api.manifesto.exception.UseCaseException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/manifesto/aprovar")
public class AprovarAgendaManifestoPresenter {

    private final AprovarAgendaManifestoUseCase aprovarAgendaManifestoUseCase;

    public AprovarAgendaManifestoPresenter(AprovarAgendaManifestoUseCase aprovarAgendaManifestoUseCase) {
        this.aprovarAgendaManifestoUseCase = aprovarAgendaManifestoUseCase;
    }

    @ApiOperation(value="aprovar agenda manifesto")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @PatchMapping
    public void aprovarAgendaManifesto(@RequestParam(name = "idManifesto") Long idManifesto) {
        try {
            aprovarAgendaManifestoUseCase.aprovarAgendaManifesto(idManifesto);
        } catch (UseCaseException e) {
            throw new PresenterException(e.getCode(), e.getMessage(), e);
        }
    }
}
