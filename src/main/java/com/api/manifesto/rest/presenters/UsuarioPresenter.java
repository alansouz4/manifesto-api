//package com.api.manifesto.rest.presenters;
//
//import com.api.manifesto.core.entity.UsuarioEntity;
//import com.api.manifesto.core.usecase.UsuarioUseCase;
//import com.api.manifesto.mappers.presentable.UsuarioPresentableMapper;
//import com.api.manifesto.rest.presentables.UsuarioPresentable;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("/usuarios")
//@RequiredArgsConstructor
//public class UsuarioPresenter {
//
//    private final UsuarioUseCase usuarioUseCase;
//    private final PasswordEncoder encoder;
//
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public UsuarioPresentable criarUsuario(@RequestBody @Valid UsuarioPresentable usuario) {
//        UsuarioEntity entity =
//                usuarioUseCase.criarUsuario(UsuarioPresentableMapper.INSTANCE.toEntity(usuario));
//        String senhaCriptogrfada = encoder.encode(usuario.getSenha());
//        entity.setSenha(senhaCriptogrfada);
//        return UsuarioPresentableMapper.INSTANCE.toPresentable(entity);
//    }
//
//}
