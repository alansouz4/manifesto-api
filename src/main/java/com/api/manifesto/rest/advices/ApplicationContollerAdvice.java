package com.api.manifesto.rest.advices;

import com.api.manifesto.exception.UseCaseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationContollerAdvice {

    @ExceptionHandler(UseCaseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleUseCaseException(UseCaseException ex){
        String messagem = ex.getMessage();
        return new ApiErrors(messagem);
    }
}
