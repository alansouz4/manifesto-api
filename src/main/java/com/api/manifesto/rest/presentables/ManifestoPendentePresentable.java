package com.api.manifesto.rest.presentables;

import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class ManifestoPendentePresentable {

    private Long id;
    private String nome;
    private LocalDateTime dataHoraCriacao;
    private EmpresaPresentable empresas;
    private StatusAgendaManifestoEnum statusAgendaManifesto;
}
