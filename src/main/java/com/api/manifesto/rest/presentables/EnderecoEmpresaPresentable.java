package com.api.manifesto.rest.presentables;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class EnderecoEmpresaPresentable {

    private Long id;

    @JsonProperty("rua_e_numero")
    private String ruaEnumero;

    @JsonProperty("bairro_empresa")
    private String bairro;

    @JsonProperty("cidade_empresa")
    private String cidade;

    @JsonProperty("estado_empresa")
    private String estado;

    @JsonProperty("pais_emprera")
    private String pais;

    @JsonProperty("data_criacao")
    private LocalDateTime dataHoraCriacao;
}
