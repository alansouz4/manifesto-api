package com.api.manifesto.rest.presentables;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class EmpresaPresentable {

    private Long id;

    @JsonProperty("nome_empresa")
    private String nome;

    @JsonProperty("data_criacao")
    private LocalDateTime dataHoraCriacao;

    private Set<EnderecoEmpresaPresentable> enderecos;
    private Collection<MotoristaPresentable> motoristas;

}
