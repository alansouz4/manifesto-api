package com.api.manifesto.rest.presentables;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class MotoristaPresentable {

    private Long id;

    @JsonProperty("nome_motorista")
    private String nome;

    @JsonProperty("rg_motorista")
    private String rg;

    @JsonProperty("placa_veiculo")
    private String placaVeiculo;

    @JsonProperty("data_criacao")
    private LocalDateTime dataHoraCriacao;

    private Collection<AjudantePresentable> ajudantes;

    @JsonProperty("habilitacao_ajudante")
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
