package com.api.manifesto.rest.presentables;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class HabilitacaoPresentable {

    private Long id;

    @JsonProperty("nome_habilitacao")
    private String nome;

    @JsonProperty("data_criacao")
    private LocalDateTime dataHoraCriacao;
}
