package com.api.manifesto.rest.presentables;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class AjudantePresentable {

    private Long id;

    @JsonProperty("nome_ajudante")
    private String nome;

    @JsonProperty("data_criacao")
    private LocalDateTime dataHoraCriacao;

    @JsonProperty("habilitacao_ajudante")
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
