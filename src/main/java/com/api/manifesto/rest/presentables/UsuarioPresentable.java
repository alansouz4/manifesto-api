package com.api.manifesto.rest.presentables;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class UsuarioPresentable {

    private Long id;

    @NotEmpty(message = "{campo.login.obrigatorio}")
    private String login;

    @NotEmpty(message = "{campo.senha.obrigatorio}")
    private String senha;

    private boolean admin;
}
