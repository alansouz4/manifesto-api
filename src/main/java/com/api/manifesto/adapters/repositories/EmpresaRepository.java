package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.EmpresaTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface EmpresaRepository extends JpaRepository<EmpresaTable, Long> {

    @Transactional
    @Modifying
    @Query("delete from EmpresaTable em where em.id = :id")
    void deletarEmpresaPorId(Long id);

    @Query("select em from EmpresaTable em where em.idManifesto = :idManifesto")
    Collection<EmpresaTable> obterEmpresaByIdManifesto(Long idManifesto);
}
