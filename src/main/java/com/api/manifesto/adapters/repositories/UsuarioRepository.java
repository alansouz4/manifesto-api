package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.UsuarioTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<UsuarioTable, Long> {

    Optional<UsuarioTable> findByLogin(String login);
}
