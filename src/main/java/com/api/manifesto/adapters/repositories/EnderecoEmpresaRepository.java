package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.EnderecoEmpresaTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;

public interface EnderecoEmpresaRepository extends JpaRepository<EnderecoEmpresaTable, Long> {

    @Query("SELECT p FROM EnderecoEmpresaTable p WHERE p.pais = :pais")
    Collection<EnderecoEmpresaTable> ObterPorPais(String pais);

    @Query("UPDATE EnderecoEmpresaTable ee SET ee = :table WHERE ee.id = :idEndereco")
    EnderecoEmpresaTable atualizarEndereco(Long idEndereco, EnderecoEmpresaTable table);

    @Query("SELECT eet FROM EnderecoEmpresaTable eet WHERE eet.empresaId = :idEmpresa")
    Set<EnderecoEmpresaTable> obterEnderecoPorIdEmpresa(Long idEmpresa);

    @Transactional
    @Modifying
    @Query("delete from EnderecoEmpresaTable em where em.id = :id")
    void deletarEnderecoPorId(Long id);
}
