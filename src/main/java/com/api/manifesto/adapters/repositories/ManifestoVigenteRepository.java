package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.ManifestoVigenteTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManifestoVigenteRepository extends JpaRepository<ManifestoVigenteTable, Long> {


}
