package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.AjudanteTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AjudanteRepository extends JpaRepository<AjudanteTable, Long> {

    @Transactional
    @Modifying
    @Query("delete from AjudanteTable em where em.id = :id")
    void deletarAjudantePorId(Long id);
}
