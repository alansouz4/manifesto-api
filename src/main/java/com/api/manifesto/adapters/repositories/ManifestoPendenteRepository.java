package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.ManifestoPendenteTable;
import com.api.manifesto.adapters.table.ManifestoVigenteTable;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ManifestoPendenteRepository extends JpaRepository<ManifestoPendenteTable, Long> {

    @Query("select mpt from ManifestoPendenteTable mpt " +
            "where mpt.id = :idManifesto and mpt.statusAgendaManifesto = :status")
    ManifestoPendenteTable obterManifestoPorIdEStatus(Long idManifesto, StatusAgendaManifestoEnum status);

    @Transactional
    @Modifying
    @Query("delete from ManifestoPendenteTable mpt where mpt.id = :id and mpt.statusAgendaManifesto = :statusAgendaManifesto")
    void deletarManifestoPendentePorIdEStatus(Long id, StatusAgendaManifestoEnum statusAgendaManifesto);

//    @Query("update ManifestoPendenteTable")
//    void updateStatusAProvadoManifesto(Long idManifesto, StatusAgendaManifestoEnum aguardandoAprovacao, StatusAgendaManifestoEnum aprovado);
}
