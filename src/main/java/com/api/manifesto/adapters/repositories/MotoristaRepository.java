package com.api.manifesto.adapters.repositories;

import com.api.manifesto.adapters.table.MotoristaTable;
import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface MotoristaRepository extends JpaRepository<MotoristaTable, Long> {

    @Query("SELECT mt.id, mt.nome FROM MotoristaTable mt WHERE mt.id = :id")
    MotoristaTable obterMotoristaPorId(@RequestParam("id") Long id);

    @Query("SELECT mt.nome FROM MotoristaTable mt")
    List<String> obterNomeMotorista();

    @Query("SELECT mt FROM MotoristaTable mt WHERE mt.empresaId = :idEmpresa")
    Collection<MotoristaTable> obterMotoristaPorIdEmpresa(Long idEmpresa);

//    @Query("SELECT mt FROM MotoristaTable mt INNER JOIN mt.habilitacao h WHERE " +
//            "(h.motorista_id = :idMotorista) AND (h.nome = :status OR h.nome :status IS NULL)")
//    @Query(value = "SELECT * FROM TB900_MOTORISTA mt INNER JOIN TB905_HABILITACAO h ON h.motorista_id = mt.ID_MOTORISTA" +
//            "WHERE h.NOME_HABILITACAO = :status OR :status IS NULL", nativeQuery = true)
//    Collection<MotoristaTable> obterMotoristaPorStatus(String status, Long idMotorista);

    @Query("SELECT mt FROM MotoristaTable mt WHERE (mt.dataHoraCriacao >= :dataInicial OR :dataInicial IS NULL) " +
            "AND (mt.dataHoraCriacao <= :dataFinal OR :dataFinal IS NULL)")
    Collection<MotoristaTable> obterMotoristaPorData(LocalDateTime dataInicial, LocalDateTime dataFinal);

    @Transactional
    @Modifying
    @Query("delete from MotoristaTable mt where mt.id = :id")
    void deletarMotorista(Long id);
}
