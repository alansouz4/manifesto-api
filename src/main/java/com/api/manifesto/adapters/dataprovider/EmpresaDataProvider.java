package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.adapters.table.EmpresaTable;
import com.api.manifesto.adapters.repositories.EmpresaRepository;
import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.core.gateway.EmpresaGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.mappers.table.EmpresaTableMapper;
import com.api.manifesto.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public class EmpresaDataProvider implements EmpresaGateway {

    Logger logger = LoggerFactory.getLogger(EmpresaDataProvider.class);

    private final EmpresaRepository repository;

    public EmpresaDataProvider(EmpresaRepository repository) {
        this.repository = repository;
    }

    public EmpresaEntity criarEmpresa(EmpresaEntity entity) {
        try {
            EmpresaTable table = repository.save(EmpresaTableMapper.INSTANCE.toTable(entity));
            logger.info("[DataProvider] | Empresa: " + table.getNome() + ", salvo no banco com sucesso!");
            return EmpresaTableMapper.INSTANCE.toEntity(table);
        } catch(DataAccessException e) {
            throw new AdapterException(422, e.getMessage(), e.getCause());
        }
    }

    public EmpresaEntity obterEmpresaById(Long id) {
        try {
            EmpresaTable table = Optional.of(repository.findById(id)).orElse(null).get();
            return EmpresaTableMapper.INSTANCE.toEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(500, MessageUtil.OBTER_EMPRESA_ADAPTER_ERRO);
        }
    }

    public Collection<EmpresaEntity> obterEmpresaByIdManifesto(Long idManifesto) {
        try {
            Collection<EmpresaTable> table = repository.obterEmpresaByIdManifesto(idManifesto);
            return EmpresaTableMapper.INSTANCE.toEntityCollection(table);
        } catch (DataAccessException e) {
            throw new AdapterException(500, MessageUtil.OBTER_EMPRESA_ADAPTER_ERRO);
        }
    }

    public void deletarEmpresaPorId(Long id) {
        try {
            repository.deletarEmpresaPorId(id);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage());
        }
    }
}
