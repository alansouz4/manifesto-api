package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.adapters.repositories.AjudanteRepository;
import com.api.manifesto.core.gateway.AjudanteGateway;
import com.api.manifesto.exception.AdapterException;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Component
public class AjudanteDataProvider implements AjudanteGateway {

    private final AjudanteRepository ajudanteRepository;

    public AjudanteDataProvider(AjudanteRepository ajudanteRepository) {
        this.ajudanteRepository = ajudanteRepository;
    }

    public void deletarAjudantePorId(Long id) {
        try {
            ajudanteRepository.deletarAjudantePorId(id);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage());
        }
    }
}
