package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.adapters.table.UsuarioTable;
import com.api.manifesto.adapters.repositories.UsuarioRepository;
import com.api.manifesto.core.entity.UsuarioEntity;
import com.api.manifesto.core.gateway.UsuarioGateway;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.mappers.table.UsuarioTableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Component
public class UsuarioDataProvider implements UsuarioGateway {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public UsuarioEntity criarUsuario(UsuarioEntity entity) {
        UsuarioTable table;
        try{
            table = repository.save(
                    UsuarioTableMapper.INSTANCE.toTable(entity));
        } catch (DataAccessException e) {
            throw new AdapterException(500, "Erro");
        }

        return UsuarioTableMapper.INSTANCE.toEntity(table);
    }

    @Override
    public UsuarioEntity obterUsuario(String login) {
        UsuarioTable table = repository.findByLogin(login).get();
        return UsuarioTableMapper.INSTANCE.toEntity(table);
    }
}
