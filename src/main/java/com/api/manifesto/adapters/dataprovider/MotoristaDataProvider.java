package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.adapters.table.MotoristaTable;
import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.core.gateway.MotoristaGateway;
import com.api.manifesto.adapters.repositories.MotoristaRepository;
import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.mappers.table.MotoristaTableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class MotoristaDataProvider implements MotoristaGateway {

    private final MotoristaRepository motoristaRepository;

    public MotoristaDataProvider(MotoristaRepository fundoRepository) {
        this.motoristaRepository = fundoRepository;
    }

    @Override
    public void criarMotorista(MotoristaEntity motoristaEntity) {
        try {
            motoristaRepository.save(MotoristaTableMapper.INSTANTE.toTable(motoristaEntity));
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage());
        }
    }

    public List<String> obterNomeMotorista() {
        try {
            return Optional.of(motoristaRepository.obterNomeMotorista()).orElse(null);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage());
        }
    }


    public MotoristaEntity obterMotoristaPorId(Long id) {
        try {
            MotoristaTable table = Optional.of(motoristaRepository.obterMotoristaPorId(id)).orElse(null);
            return MotoristaTableMapper.INSTANTE.toEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage());
        }
    }

    public Collection<MotoristaEntity> obterMotoristaPorIdEmpresa(Long idEmpresa) {
        try {
            Collection<MotoristaTable> motoristaTable =
                    motoristaRepository.obterMotoristaPorIdEmpresa(idEmpresa);
            if (motoristaTable == null || motoristaTable.isEmpty())
                return null; // retorna noContent
            return MotoristaTableMapper.INSTANTE.toEntitySet(motoristaTable);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage());
        }
    }

//    public Collection<MotoristaEntity> obterMotoristaPorStatus(String status, Long idMotorista) {
//        try {
//            Collection<MotoristaTable> motoristaTables =
//                    motoristaRepository.obterMotoristaPorStatus(status, idMotorista);
//            if (motoristaTables == null || motoristaTables.isEmpty())
//                return null;
//            return MotoristaTableMapper.INSTANTE.toEntitySet(motoristaTables);
//        } catch (DataAccessException e) {
//            throw new AdapterException(422, e.getMessage());
//        }
//    }

    public Collection<MotoristaEntity> obterMotoristaPorData(LocalDateTime dataInicial, LocalDateTime dataFinal) {

        try {
            Collection<MotoristaTable> motoristaTables =
                    motoristaRepository.obterMotoristaPorData(dataInicial, dataFinal);
            if (motoristaTables == null || motoristaTables.isEmpty())
                return null;
            return MotoristaTableMapper.INSTANTE.toEntitySet(motoristaTables);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage());
        }
    }

    public void deletarMotorista(Long id) {
        try {
            motoristaRepository.deletarMotorista(id);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage());
        }
    }
}
