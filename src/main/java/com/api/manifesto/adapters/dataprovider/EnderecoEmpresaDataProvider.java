package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.core.gateway.EnderecoEmpresaGateway;
import com.api.manifesto.adapters.table.EnderecoEmpresaTable;
import com.api.manifesto.adapters.repositories.EnderecoEmpresaRepository;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.mappers.table.EnderecoEmpresaTableMapper;
import com.api.manifesto.util.MessageUtil;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class EnderecoEmpresaDataProvider implements EnderecoEmpresaGateway {

    private EnderecoEmpresaRepository repository;

    public EnderecoEmpresaDataProvider(EnderecoEmpresaRepository repository) {
        this.repository = repository;
    }

    public EnderecoEmpresaEntity criarEnderecoEmpresa(EnderecoEmpresaEntity entity) {
        EnderecoEmpresaTable table = repository.save(EnderecoEmpresaTableMapper.INSTANCE.toTable(entity));

        return EnderecoEmpresaTableMapper.INSTANCE.toEntity(table);
    }

    public EnderecoEmpresaEntity obterEnderecoById(Long idEndereco) {
        try {
            EnderecoEmpresaTable table =
                    Optional.of(repository.findById(idEndereco)).orElseThrow(null).get();
            return EnderecoEmpresaTableMapper.INSTANCE.toEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(500, MessageUtil.OBTER_ENDERECO_ADAPTER_ERRO);
        }
    }

    @Override
    public Set<EnderecoEmpresaEntity> obterEnderecoPorIdEmpresa(Long idEmpresa) {
        Set<EnderecoEmpresaTable> byIdEmpresa = repository.obterEnderecoPorIdEmpresa(idEmpresa);
        return EnderecoEmpresaTableMapper.INSTANCE.toEntitySet(byIdEmpresa);
    }

    public Collection<EnderecoEmpresaEntity> findPais(String pais) {
        try {

            Collection<EnderecoEmpresaTable> table = repository.ObterPorPais( pais );
            if (table == null) {
                throw new AdapterException(404, MessageUtil.NOTFOUND_PAIS_USECASE);
            }
            return EnderecoEmpresaTableMapper.INSTANCE.toEntityCollection(table);

        } catch (DataAccessException e) {
            throw new AdapterException(500, e.getMessage(), e);
        }
    }

    public List<EnderecoEmpresaEntity> findAll() {
        List<EnderecoEmpresaTable> table;
        try {
            table = repository.findAll();
            if (table.isEmpty()) {
                throw new AdapterException(404, MessageUtil.NOTFOUND_ENDERECO_USECASE);
            }
        } catch (DataAccessException e) {
            throw new AdapterException(500, MessageUtil.OBTER_ENDERECO_ADAPTER_ERRO);
        }
        return EnderecoEmpresaTableMapper.INSTANCE.toEntityList(table);
    }

    public EnderecoEmpresaEntity alterarPais(Long idEndereco, EnderecoEmpresaEntity entity) {
        EnderecoEmpresaTable table =
                repository.atualizarEndereco(idEndereco, EnderecoEmpresaTableMapper.INSTANCE.toTable(entity));
        if(table == null || table.equals(""))
            throw new AdapterException(404, "Vazio");
        return EnderecoEmpresaTableMapper.INSTANCE.toEntity(table);
    }

    @Override
    public void deletarEnderecoPorId(Long id) {
        try {
            repository.deletarEnderecoPorId(id);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage(), e);
        }
    }
}
