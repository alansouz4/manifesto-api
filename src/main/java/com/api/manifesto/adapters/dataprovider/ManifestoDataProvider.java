package com.api.manifesto.adapters.dataprovider;

import com.api.manifesto.adapters.repositories.ManifestoVigenteRepository;
import com.api.manifesto.adapters.table.ManifestoPendenteTable;
import com.api.manifesto.adapters.repositories.ManifestoPendenteRepository;
import com.api.manifesto.adapters.table.ManifestoVigenteTable;
import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.entity.ManifestoVigenteEntity;
import com.api.manifesto.core.gateway.ManifestoGateway;
import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import com.api.manifesto.exception.AdapterException;
import com.api.manifesto.mappers.table.ManifestoTableMapper;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Component
public class ManifestoDataProvider implements ManifestoGateway {

    private final ManifestoPendenteRepository manifestoPendenteRepository;
    private final ManifestoVigenteRepository manifestoVigenteRepository;

    public ManifestoDataProvider(ManifestoPendenteRepository manifestoPendenteRepository,
                                 ManifestoVigenteRepository manifestoVigenteRepository) {
        this.manifestoPendenteRepository = manifestoPendenteRepository;
        this.manifestoVigenteRepository = manifestoVigenteRepository;
    }

    public ManifestoPendenteEntity criarManifesto(ManifestoPendenteEntity entity) {
        try {
            ManifestoPendenteTable table = manifestoPendenteRepository.save(ManifestoTableMapper.INSTANCE.toTable(entity));
            return ManifestoTableMapper.INSTANCE.toEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage(), e);
        }
    }

    public ManifestoPendenteEntity obterManifestoPorIdEStatus(Long id, StatusAgendaManifestoEnum status) {
        try {
            ManifestoPendenteTable table =
                    manifestoPendenteRepository.obterManifestoPorIdEStatus(id, status);
            return ManifestoTableMapper.INSTANCE.toEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(422, e.getMessage(), e);
        }
    }

    public void deletarManifestoPendentePorIdEStatus(Long id, StatusAgendaManifestoEnum status) {
        try {
            if (status.equals(StatusAgendaManifestoEnum.APROVADO)){
                throw new AdapterException(503, "Erro: Manifesto pendente esta como status aprovado");
            }
            manifestoPendenteRepository.deletarManifestoPendentePorIdEStatus(id, status);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage(), e);
        }
    }

    public ManifestoVigenteEntity salvarManifestoVigente(ManifestoVigenteEntity entity) {
        try {
            ManifestoVigenteTable table = manifestoVigenteRepository.save(
                    ManifestoTableMapper.INSTANCE.toTableVigente(entity));

            return ManifestoTableMapper.INSTANCE.toVigenteEntity(table);
        } catch (DataAccessException e) {
            throw new AdapterException(503, e.getMessage(), e);
        }
    }
}
