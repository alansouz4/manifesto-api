package com.api.manifesto.adapters.table;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "TB900_MOTORISTA")
public class MotoristaTable implements Serializable {

    private final static long serialVersionUid = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MOTORISTA")
    private Long id;

    @Column(name = "EMPRESA_ID")
    private Long empresaId;

    @Column(name = "NOME_MOTORISTA")
    private String nome;

    @Column(name = "RG")
    private String rg;

    @Column(name = "PLACA_VEICULO")
    private String placaVeiculo;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Column(name = "DATA_MANUTENCAO")
    private LocalDateTime dataHoraManutencao;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "MOTORISTA_ID")
    private Collection<AjudanteTable> ajudantes;

    @Enumerated(EnumType.STRING)
    @Column(name = "HABILITACAO")
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
