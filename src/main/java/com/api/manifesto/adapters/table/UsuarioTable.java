package com.api.manifesto.adapters.table;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity
@Table(name = "usuario")
public class UsuarioTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String login;

    @Column
    private String senha;

    @Column
    private boolean admin;
}
