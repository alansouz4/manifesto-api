package com.api.manifesto.adapters.table;

import com.api.manifesto.enums.CategoriaHabilitacaoEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "TB905_AJUDANTE")
public class AjudanteTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_AJUDANTE")
    private Long id;

    @Column(name = "MOTORISTA_ID")
    private Long motoristaId;

    @Column(name = "NOME_AJUDANTE")
    private String nome;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Column(name = "DATA_MANUTENCAO")
    private LocalDateTime dataHoraManutencao;

    @Enumerated(EnumType.STRING)
    @Column(name = "HABILITACAO")
    private CategoriaHabilitacaoEnum categoriaHabilitacaoEnum;
}
