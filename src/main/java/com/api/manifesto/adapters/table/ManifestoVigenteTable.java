package com.api.manifesto.adapters.table;

import com.api.manifesto.enums.StatusAgendaManifestoEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "TB904_MANIFESTO_VGNT")
public class ManifestoVigenteTable {

    @Id
    @Column(name = "ID_MANIFESTO")
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS_AGENDA_MANIFESTO")
    private StatusAgendaManifestoEnum statusAgendaManifesto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANIFESTO_ID")
    private EmpresaTable empresas;
}
