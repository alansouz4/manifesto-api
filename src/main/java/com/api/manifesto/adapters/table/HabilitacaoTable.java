package com.api.manifesto.adapters.table;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity
@Table(name = "TB906_HABILITACAO")
public class HabilitacaoTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_HABILITACAO")
    private Long id;

    @Column(name = "MOTORISTA_ID")
    private Long motoristaId;

    @Column(name = "NOME_HABILITACAO")
    private String nome;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;
}
