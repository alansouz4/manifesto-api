package com.api.manifesto.adapters.table;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "TB901_ENDERECO_EMPRESA")
public class EnderecoEmpresaTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ENDERECO")
    private Long id;

    @Column(name = "EMPRESA_ID")
    private Long empresaId;

    @Column(name = "RUA_E_NUMERO")
    private String ruaEnumero;

    @Column(name = "BAIRRO")
    private String bairro;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "PAIS")
    private String pais;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Column(name = "DATA_MANUTENCAO")
    private LocalDateTime dataHoraManutencao;
}
