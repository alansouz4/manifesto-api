package com.api.manifesto.adapters.table;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

@Builder
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "TB902_EMPRESA")
public class EmpresaTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMPRESA")
    private Long id;

    @Column(name = "ID_MANIFESTO")
    private Long idManifesto;

    @Column(name = "NOME_EMPRESA", updatable = false, length = 100, nullable = true)
    private String nome;

    @Column(name = "DATA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Column(name = "DATA_MANUTENCAO")
    private LocalDateTime dataHoraManutencao;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPRESA_ID")
    private Set<EnderecoEmpresaTable> enderecos;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPRESA_ID")
    private Collection<MotoristaTable> motoristas;
}
