package com.api.manifesto.mappers.table;

import com.api.manifesto.adapters.table.UsuarioTable;
import com.api.manifesto.core.entity.UsuarioEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UsuarioTableMapper {

    UsuarioTableMapper INSTANCE = Mappers.getMapper(UsuarioTableMapper.class);

    UsuarioTable toTable(UsuarioEntity entity);

    UsuarioEntity toEntity(UsuarioTable table);
}
