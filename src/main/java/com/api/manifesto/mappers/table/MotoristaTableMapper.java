package com.api.manifesto.mappers.table;

import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.adapters.table.MotoristaTable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface MotoristaTableMapper {

    MotoristaTableMapper INSTANTE = Mappers.getMapper(MotoristaTableMapper.class);

    MotoristaTable toTable(MotoristaEntity entity);

    MotoristaEntity toEntity(MotoristaTable motoristaTable);

    Collection<MotoristaEntity> toEntitySet(Collection<MotoristaTable> motoristaTable);
}
