package com.api.manifesto.mappers.table;

import com.api.manifesto.adapters.table.ManifestoPendenteTable;
import com.api.manifesto.adapters.table.ManifestoVigenteTable;
import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.core.entity.ManifestoVigenteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ManifestoTableMapper {

    ManifestoTableMapper INSTANCE = Mappers.getMapper(ManifestoTableMapper.class);

    ManifestoPendenteTable toTable(ManifestoPendenteEntity entity);

    ManifestoPendenteEntity toEntity(ManifestoPendenteTable table);

    ManifestoVigenteTable toTableVigente(ManifestoVigenteEntity entity);

    ManifestoVigenteEntity pendenteToEntityVigente(ManifestoPendenteEntity entityPendente);

    @Mapping(source = "empresas.id", target = "idsEmpresas")
    ManifestoVigenteEntity toVigenteEntity(ManifestoVigenteTable table);
}
