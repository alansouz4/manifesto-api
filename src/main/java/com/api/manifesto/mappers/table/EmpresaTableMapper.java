package com.api.manifesto.mappers.table;

import com.api.manifesto.adapters.table.EmpresaTable;
import com.api.manifesto.core.entity.EmpresaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface EmpresaTableMapper {

    EmpresaTableMapper INSTANCE = Mappers.getMapper(EmpresaTableMapper.class);

    EmpresaTable toTable(EmpresaEntity entity);

    EmpresaEntity toEntity(EmpresaTable table);

    Collection<EmpresaEntity> toEntityCollection(Collection<EmpresaTable> table);
}
