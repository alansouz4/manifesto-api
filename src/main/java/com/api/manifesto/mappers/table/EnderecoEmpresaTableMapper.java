package com.api.manifesto.mappers.table;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.adapters.table.EnderecoEmpresaTable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "string")
public interface EnderecoEmpresaTableMapper {

    EnderecoEmpresaTableMapper INSTANCE = Mappers.getMapper(EnderecoEmpresaTableMapper.class);

    EnderecoEmpresaTable toTable(EnderecoEmpresaEntity entity);

    EnderecoEmpresaEntity toEntity(EnderecoEmpresaTable table);

    List<EnderecoEmpresaEntity> toEntityList(List<EnderecoEmpresaTable> table);

    Collection<EnderecoEmpresaEntity> toEntityCollection(Collection<EnderecoEmpresaTable> table);

    Set<EnderecoEmpresaEntity> toEntitySet(Set<EnderecoEmpresaTable> byIdEmpresa);
}
