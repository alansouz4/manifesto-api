package com.api.manifesto.mappers;

import com.api.manifesto.core.entity.ManifestoPendenteEntity;
import com.api.manifesto.rest.presentables.ManifestoPendentePresentable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ManifestoPresentableMapper {

    ManifestoPresentableMapper INSTANCE = Mappers.getMapper(ManifestoPresentableMapper.class);

    ManifestoPendentePresentable toPresentable(ManifestoPendenteEntity entity);

    ManifestoPendenteEntity toEntity(ManifestoPendentePresentable presentable);
}
