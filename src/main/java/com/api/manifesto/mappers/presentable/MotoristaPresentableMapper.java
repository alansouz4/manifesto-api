package com.api.manifesto.mappers.presentable;

import com.api.manifesto.core.entity.MotoristaEntity;
import com.api.manifesto.mappers.table.MotoristaTableMapper;
import com.api.manifesto.rest.presentables.MotoristaPresentable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface MotoristaPresentableMapper {

    MotoristaPresentableMapper INSTANTE = Mappers.getMapper(MotoristaPresentableMapper.class);

    MotoristaEntity toEntity(MotoristaPresentable httpModel);

    MotoristaPresentable toPresentable(MotoristaEntity entity);

    Collection<MotoristaPresentable> toPresentableCollection(Collection<MotoristaEntity> entity);
}
