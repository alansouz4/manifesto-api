package com.api.manifesto.mappers.presentable;

import com.api.manifesto.core.entity.UsuarioEntity;
import com.api.manifesto.rest.presentables.UsuarioPresentable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UsuarioPresentableMapper {

    UsuarioPresentableMapper INSTANCE = Mappers.getMapper(UsuarioPresentableMapper.class);

    UsuarioEntity toEntity(UsuarioPresentable table);

    UsuarioPresentable toPresentable(UsuarioEntity entity);
}
