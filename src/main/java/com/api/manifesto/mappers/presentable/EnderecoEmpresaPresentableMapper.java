package com.api.manifesto.mappers.presentable;

import com.api.manifesto.core.entity.EnderecoEmpresaEntity;
import com.api.manifesto.rest.presentables.EnderecoEmpresaPresentable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "Spring")
public interface EnderecoEmpresaPresentableMapper {

    EnderecoEmpresaPresentableMapper INSTANCE = Mappers.getMapper(EnderecoEmpresaPresentableMapper.class);

    EnderecoEmpresaEntity toEntity(EnderecoEmpresaPresentable httpModel);

    EnderecoEmpresaPresentable toHttpModel(EnderecoEmpresaEntity entity);
}
