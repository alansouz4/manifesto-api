package com.api.manifesto.mappers.presentable;

import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.rest.presentables.EmpresaPresentable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface EmpresaPresentableMapper {

    EmpresaPresentableMapper INSTANCE = Mappers.getMapper(EmpresaPresentableMapper.class);

    EmpresaEntity toEntity(EmpresaPresentable httpModel);

    EmpresaPresentable toHttpModel(EmpresaEntity entity);
}
