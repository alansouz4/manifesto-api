package com.api.manifesto.dataprovider;

import com.api.manifesto.adapters.dataprovider.EmpresaDataProvider;
import com.api.manifesto.core.entity.EmpresaEntity;
import com.api.manifesto.adapters.table.EmpresaTable;
import com.api.manifesto.adapters.repositories.EmpresaRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class EmpresDataProviderTest {

    @InjectMocks
    private EmpresaDataProvider dataProvider;

    @Mock
    private EmpresaRepository repository;

    @Test
    public void criarEmpresa() {

        Mockito.when(repository.save(Mockito.any())).thenReturn(table());

        dataProvider.criarEmpresa(entity());

        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any());
    }

    private EmpresaEntity entity() {
       return EmpresaEntity.builder()
                .id(1L)
                .build();
    }

    private EmpresaTable table() {
        return EmpresaTable.builder()
                .id(1L)
                .build();
    }

}
