//package com.api.manifesto.core.usecase;
//
//import com.api.manifesto.core.entity.EmpresaEntity;
//import com.api.manifesto.core.gateway.EmpresaGateway;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//public class CriarEmpresaUseCaseTest {
//
//    @InjectMocks
//    private EmpresaUseCase useCase;
//
//    @Mock
//    private EmpresaGateway gateway;
//
//    private static final EmpresaEntity ENMPRESA_ENTITY = EmpresaEntity.builder()
//            .id(1L)
//            .build();
//
//    @Test
//    public void criarEmpresa() {
//
//        Mockito.when(gateway.obterEmpresaById(123L)).thenReturn(ENMPRESA_ENTITY);
//
//        useCase.criarEmpresa(ENMPRESA_ENTITY);
//
//        Mockito.verify(gateway, Mockito.times(1)).criarEmpresa(ENMPRESA_ENTITY);
//    }
//
//}
